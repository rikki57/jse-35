package ru.nlmk.study.jse35.streams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
/*        List<Integer> numbers = Arrays.asList(-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5);
        int count = 0;
        for (int i : numbers){
            if (i > 0){
                count++;
            }
        }*/

        /*        long count = Stream.of(-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5).filter(w -> w > 0).count();
        System.out.println(count);*/

/*        List<Integer> list = Stream.of(-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5).skip(6).collect(Collectors.toList());
        print(list);*/

/*        List<User> users = Arrays.asList(
                new User("Иван", "Петров", 1995),
                new User("Петр", "Сидоров", 1992),
                new User("Сидор", "Иванов", 1989)
        );
        List<AnotherUser> list = users.stream().map(UserMapper::map).collect(Collectors.toList());
        print(list);*/

/*        List<String> list = Stream.of("Иван", "Петр", "Сидор").map(String::toUpperCase)
                .peek(e ->System.out.println(", " + e.toLowerCase()))
                .collect(Collectors.toList());
        print(list);*/

        List<Integer> numbers = Arrays.asList(-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5);
        List<Integer> result = numbers.parallelStream().filter(w -> w > 0).collect(Collectors.toList());
        System.out.println(result);

        /*        List<String> list = Stream.of("Петр", "Сидор", "Иван").sorted().collect(Collectors.toList());
        print(list);*/

/*        Stream.of("Иннокентий", "Анатолий", "Петр", "Сидор", "Иван")
                .sorted((o1, o2) -> o1.length() > o2.length() ? -1 : 1)
                .forEach(System.out::println);*/
/*        List<User> users = Arrays.asList(
                new User("Иван", "Петров", 1995),
                new User("Петр", "Сидоров", 1992),
                new User("Сидор", "Иванов", 1989)
        );
        users.stream()
                .map(UserMapper::map)
                .map(user -> user.getFullName())
                .sorted((o1, o2) -> o1.length() > o2.length() ? -1 : 1)
                .forEach(System.out::println);*/
    }

    private static void print(List list){
        list.forEach(System.out::println);
    }
}
