package ru.nlmk.study.jse35.streams;

public class User {
    private String name;
    private String familyName;
    private Integer yearOfBirth;

    public User(String name, String familyName, Integer yearOfBirth) {
        this.name = name;
        this.familyName = familyName;
        this.yearOfBirth = yearOfBirth;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public Integer getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(Integer yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }
}
