package ru.nlmk.study.jse35.streams;

public class UserMapper {
    public static AnotherUser map(User user){
        return new AnotherUser(
                user.getName() + " " + user.getFamilyName(),
                2020 - user.getYearOfBirth()
        );
    }
}
