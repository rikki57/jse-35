package ru.nlmk.study.jse35.optional;

import java.util.Optional;

public class Repository {
    public Optional<User> getUserByID(Long id){
        //ищем пользователя
        //Если нашли, то:
            //return Optional.of(new User("Sergey", 25));
        //Если не нашли, то:
        return Optional.empty();
    }
}
