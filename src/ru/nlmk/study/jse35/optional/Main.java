package ru.nlmk.study.jse35.optional;

import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        Repository repository = new Repository();
        Optional<User> user = repository.getUserByID(1L);
        System.out.println(user.orElseThrow(() -> new IllegalArgumentException()).getName());
    }
}
