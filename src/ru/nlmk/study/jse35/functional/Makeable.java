package ru.nlmk.study.jse35.functional;

@FunctionalInterface
public interface Makeable {
    String make(String input);
}
