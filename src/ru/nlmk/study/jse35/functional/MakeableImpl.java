package ru.nlmk.study.jse35.functional;

public class MakeableImpl implements Makeable{
    @Override
    public String make(String input) {
        return "Make " + input;
    }
}
