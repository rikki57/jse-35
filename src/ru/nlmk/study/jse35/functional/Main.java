package ru.nlmk.study.jse35.functional;

public class Main {
    public static void main(String[] args) {
        Makeable makeable1 = new MakeableImpl();
        System.out.println(makeable1.make("input"));

        Makeable makeable2 = new Makeable() {
            @Override
            public String make(String input) {
                return "Make " + input;
            }
        };
        System.out.println(makeable2.make("input"));

        Makeable makeable3 = input -> "Make " + input;
        System.out.println(makeable3.make("input"));
    }
}
