package ru.nlmk.study.jse35.links;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8);
        //numbers.forEach(a -> System.out.println(a));
        numbers.forEach(System.out::println);
    }
}
