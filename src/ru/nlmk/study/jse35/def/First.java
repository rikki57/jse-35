package ru.nlmk.study.jse35.def;

public interface First {
    default String method(String input){
        return input + " first";
    }
}
