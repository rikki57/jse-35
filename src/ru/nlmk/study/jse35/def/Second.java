package ru.nlmk.study.jse35.def;

public interface Second {
    default String method(String input){
        return input + " second";
    }
}
